from django.db import models
from django.utils import timezone
from ckeditor_uploader.fields import RichTextUploadingField


# Create your models here.


class Prog(models.Model):
    author = models.ForeignKey('auth.User')
    image = models.FileField(upload_to='media/%Y/%m/%d/', null=True, blank=True, verbose_name="Обложка")
    title = models.CharField(max_length=200)
    text = RichTextUploadingField(blank=True, default='')
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    

    class Meta:
        verbose_name = 'Программирование'
        verbose_name_plural = 'Программирование' 
    

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title





class Contacts(models.Model):
    autor = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = RichTextUploadingField(blank=True, default='')
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    class Meta:
        verbose_name = 'Контакты'
        verbose_name_plural = 'Контакты'          


    def publish(self):
        self.published_date = timezone.now()
        self.save()



    def __unicode__(self):
        return u'{c}'.format(c=self.title)




class Glavnaya(models.Model):
    autor = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = RichTextUploadingField(blank=True, default='')
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    class Meta:
        verbose_name = 'Главная страница'
        verbose_name_plural = 'Главная страница'          


    def publish(self):
        self.published_date = timezone.now()
        self.save()


    def __unicode__(self):
        return u'{c}'.format(c=self.title)