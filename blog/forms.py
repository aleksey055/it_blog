# -*- coding: utf-8 -*-
from django import forms
from .models import Prog
from django.forms import ModelForm

class PostForm(forms.ModelForm):

    class Meta:
        model = Prog
        fields = ['title', 'image']
