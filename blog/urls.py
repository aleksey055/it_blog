# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.glavnaya, name='glavnaya'),


    url(r'^prog_list', views.prog_list, name='prog_list'),
    url(r'^prog_detail/(?P<pk>[0-9]+)/$', views.prog_detail, name='prog_detail'),
    url(r'^progs/', views.progs, name='progs'),



    url(r'^contacts', views.contacts, name='contacts'),

]
