# -*- coding: utf-8 -*-

from django.shortcuts import render, get_object_or_404, redirect
from django.template.loader import get_template
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.utils import timezone
from .forms import PostForm
from .models import Prog
from .models import Glavnaya
from .models import Contacts

# Create your views here.


from slider.models import Document
from slider.forms import DocumentForm


from slider.models import Document2
from slider.forms import DocumentForm2


from slider.models import Document3
from slider.forms import DocumentForm3






def glavnaya(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        form2 = DocumentForm2(request.POST, request.FILES)
        form3 = DocumentForm3(request.POST, request.FILES)


        if form.is_valid() and form2.is_valid():
            newdoc = Document(docfile=request.FILES['docfile'])
            newdoc.save()


            newdoc2 = Document2(docfile2=request.FILES['docfile2'])
            newdoc2.save()

            newdoc3 = Document3(docfile3=request.FILES['docfile3'])
            newdoc3.save()


            # Redirect to the document list after POST
            return redirect('/')
    else:
        form = DocumentForm()  # A empty, unbound form
        form2 = DocumentForm()  # A empty, unbound form
        form3 = DocumentForm()  # A empty, unbound form        
    # Load documents for the list page
    documents = Document.objects.all()
    documents2 = Document2.objects.all()
    documents3 = Document3.objects.all()
    glav = get_object_or_404(Glavnaya)
    return render(request, 'blog/glavnaya.html', {'glav': glav,'documents': documents, 'documents2': documents2, 'documents3': documents3})






def prog_list(request):
    posts = Prog.objects.all()
    return render(request, 'blog/prog_list.html', {'posts': posts})


def prog_detail(request, pk):
    post = get_object_or_404(Prog, pk=pk)
    return render(request, 'blog/prog_detail.html', {'post': post})



def progs(request):

    if request.method == "POST":
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('prog_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/prog_form.html', {'form': form})







def contacts(request):
    cont = get_object_or_404(Contacts)
    return render(request, 'blog/contacts.html', {'cont': cont})